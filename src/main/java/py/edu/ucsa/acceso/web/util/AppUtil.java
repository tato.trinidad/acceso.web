package py.edu.ucsa.acceso.web.util;
public class AppUtil{
	public static String buildUrl(String host, String apiUrl){
		StringBuffer st = new StringBuffer();
		if (host != null && !host.isEmpty() 
				&& apiUrl != null && !apiUrl.isEmpty()){
			st.append(host).append(apiUrl);
		}
		return st.toString();
	}
}
