package py.edu.ucsa.acceso.web.exceptions;

public class ServiceException extends Exception {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1587910733988942893L;
    
    private ErrorCode errorCode;
	
	public ServiceException(ErrorCode errorCode) {
		super();
		this.errorCode = errorCode;
	}
	
	public ServiceException(ErrorCode errorCode, String message, Throwable cause) {
		super(message, cause);
		this.errorCode = errorCode;
	}
	
	public ServiceException(ErrorCode errorCode, String message) {
		super(message);
		this.errorCode = errorCode;
	}
	
	public ServiceException(ErrorCode errorCode, Throwable cause) {
		super(cause);
		this.errorCode = errorCode;
	}

	public ErrorCode getErrorCode(){
		return this.errorCode;
	}
	
	public String getErrorMessage(){
		StringBuilder sb = new StringBuilder();
		sb.append(getErrorCode()).append("=").append(this.getMessage()).append("\n");
		return sb.toString();
	}
	
	
	public ServiceException(ErrorCode errorCode, Throwable cause, String message, Object...args) {
		super(String.format(message, args), cause);
		this.errorCode = errorCode;
	}
	
	public ServiceException(ErrorCode errorCode, String message, Object...args) {
		super(String.format(message, args));
		this.errorCode = errorCode;
	}
	
}
