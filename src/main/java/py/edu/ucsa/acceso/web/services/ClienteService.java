package py.edu.ucsa.acceso.web.services;

import java.util.List;

import py.edu.ucsa.acceso.web.dto.ClienteDTO;

public interface ClienteService {

    public List<ClienteDTO> listar();

	public ClienteDTO getById(Long id);

}
