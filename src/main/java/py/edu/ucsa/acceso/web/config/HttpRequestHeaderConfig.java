package py.edu.ucsa.acceso.web.config;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.client.methods.HttpRequestBase;

public abstract class HttpRequestHeaderConfig {

   public void setAuthorizationHeader(HttpRequestBase request){
      String token = ConfigurationFactory.getConfig().getUsername() + ":" + 
                    ConfigurationFactory.getConfig().getPassword();
      Base64 enc = new Base64();
      String encodedAuthorization = enc.encodeAsString(token.getBytes());
      request.setHeader("Authorization", "Basic " + encodedAuthorization); 
   }

}
