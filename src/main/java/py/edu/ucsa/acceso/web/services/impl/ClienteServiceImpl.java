package py.edu.ucsa.acceso.web.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import py.edu.ucsa.acceso.web.dto.ClienteDTO;
import py.edu.ucsa.acceso.web.services.ClienteService;
import py.edu.ucsa.acceso.web.util.ClienteRESTUtil;
import py.edu.ucsa.acceso.web.util.JSONTranslator;

@Service
public class ClienteServiceImpl extends ClienteRESTUtil implements ClienteService {

	private static final long serialVersionUID = -7808009560749461244L;
	private static Logger log = LoggerFactory.getLogger(ClienteServiceImpl.class);
	private JSONTranslator<ClienteDTO> translator = new JSONTranslator<ClienteDTO>();
	private final static String moduleUri = "/cliente";

	@Override
	public List<ClienteDTO> listar() {
		try {
			String jsondata = super.list(projectURI + moduleUri);
			return translator.toList(jsondata, ClienteDTO.class);
		} catch (Exception e) {
			log.error(e.getMessage());
		}

		return null;
	}

	@Override
	public ClienteDTO getById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}
}
